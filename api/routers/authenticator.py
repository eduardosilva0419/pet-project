from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from authenticator import authenticator
from queries.accounts import (
    AccountIn,
    AccountRepo,
    DuplicateAccountError,
)

from models.accounts import (
    AccountForm,
    AccountToken,
    HttpError,
)


router = APIRouter()


@router.post("/api/accounts", response_model=AccountToken | HttpError)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    repo: AccountRepo = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        # MONGO?
        account = repo.create(info, hashed_password)
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Account already exists.",
        )
    form = AccountForm(username=info.email)
    token = await authenticator.login(response, request, form, repo)
    return AccountToken(account=account, **token.dict())
