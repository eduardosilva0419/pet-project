from fastapi import (
    Depends,
    APIRouter,
)
from authenticator import authenticator
from models.projects import ProjectsAndTasks
from models.tasks import (
    TasksIn,
    TasksOut,
    AllTasksOut,
)
from queries.tasks import TasksRepo
from queries.projects import ProjectsRepo


router = APIRouter()


@router.post("/api/tasks", response_model=TasksOut)
async def create_task(
    task_in: TasksIn,
    repo: TasksRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    task = repo.create(task_in, account_data["id"])
    return task


@router.get("/api/tasks/mine", response_model=AllTasksOut)
async def get_all_my_tasks(
    repo: TasksRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    tasks = repo.get_all_tasks(account_data["id"])
    return {"tasks": tasks}


@router.get("/api/tasks/{task_id}", response_model=TasksOut)
async def get_my_task(
    task_id: int,
    repo: TasksRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    task = repo.get_task(task_id, account_data["id"])
    return task


@router.delete("/api/tasks/{task_id}")
async def delete_my_task(
    task_id: int,
    repo: TasksRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    user_task = repo.delete_task(task_id, account_data["id"])
    if user_task is not None:
        return {"message": "Task deleted"}
    return {"message": "Task not found"}


@router.put("/api/tasks/{task_id}", response_model=TasksOut)
async def update_my_task(
    task_id: int,
    info: TasksIn,
    repo: TasksRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    user_task_update = repo.update_task(task_id, account_data["id"], info)
    if user_task_update is not None:
        return user_task_update
    return {"message", "Task Not Found"}


@router.get(
    "/api/projects/{project_id}/tasks",
    response_model=ProjectsAndTasks,
)
async def get_my_tasks_for_project(
    project_id: int,
    repo: ProjectsRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    projects_with_tasks = repo.get_project_with_tasks(
        project_id,
        account_data["id"],
    )
    return projects_with_tasks
