from fastapi import (
    Depends,
    APIRouter,
)
from models.projects import (
    ProjectsIn,
    ProjectsOut,
    AllProjectsOut,
    ProjectId,
)
from queries.projects import ProjectsRepo
from authenticator import authenticator

router = APIRouter()


@router.post("/api/projects/mine", response_model=ProjectsOut)
async def create_my_project(
    project_in: ProjectsIn,
    repo: ProjectsRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    project = repo.create(project_in, account_data["id"])
    return project


@router.get("/api/projects/mine", response_model=AllProjectsOut)
async def get_all_my_projects(
    repo: ProjectsRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    projects = repo.get_all_projects(account_data["id"])
    return {"projects": projects}


@router.get("/api/projects/{project_id}", response_model=ProjectsOut)
async def get_my_project(
    project_id: ProjectId,
    repo: ProjectsRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    project = repo.get_project(project_id, account_data["id"])
    return project


@router.delete("/api/projects/{project_id}")
async def delete_my_project(
    project_id: ProjectId,
    repo: ProjectsRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    user_project = repo.delete_project(project_id, account_data["id"])
    if user_project is not None:
        return {"message": "Project deleted"}
    return {"message": "Project not found"}


@router.put("/api/projects/{project_id}", response_model=ProjectsOut)
async def update_my_project(
    project_id: ProjectId,
    info: ProjectsIn,
    repo: ProjectsRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    user_project_update = repo.update_project(
        project_id,
        account_data["id"],
        info,
    )
    if user_project_update is not None:
        return user_project_update
    return {"message": "Project not found"}
