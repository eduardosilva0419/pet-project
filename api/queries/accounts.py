from queries.pool import pool
from models.accounts import AccountOut


class AccountRepo:
    def create(self, info, hashed_password):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                INSERT INTO accounts
                    (username, password)
                VALUES
                (%s, %s)
                RETURNING id;
                """,
                    [info.username, hashed_password],
                )

                id = result.fetchone()[0]
                old_data = info.dict()
                account_out = AccountOut(id=id, **old_data)
                return account_out

    def get(self, username: str):
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                SELECT id, username, password
                FROM accounts
                WHERE username = %s
                    """,
                    [username],
                )
                record = None
                row = db.fetchone()
                if row is not None:
                    record = {}
                    for i, col in enumerate(db.description):
                        record[col.name] = row[i]
                return record
