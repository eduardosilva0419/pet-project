from queries.pool import pool
from models.tasks import TasksIn, TasksOut


class TasksRepo:
    def create(self, tasks_in: TasksIn, user_id: int):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                INSERT INTO tasks
                    (title, description, due_date, is_completed, project_id)
                VALUES
                (%s, %s, %s, %s, %s)
                RETURNING id, created
                """,
                    [
                        tasks_in.title,
                        tasks_in.description,
                        tasks_in.due_date,
                        tasks_in.is_completed,
                        tasks_in.project_id,
                    ],
                )
                results = result.fetchone()
                id = results[0]
                created = results[1]
                old_data = tasks_in.dict()
                tasks_out = TasksOut(id=id, created=created, **old_data)
                return tasks_out

    def get_all_tasks(self, user_id):
        with pool.connection() as conn:
            with conn.cursor() as db:
                results = db.execute(
                    """
                SELECT t.title, t.description, t.due_date, t.is_completed
                , t.project_id, t.id, t.created
                FROM tasks t
                JOIN projects p
                ON t.project_id = p.id
                WHERE p.user_id = %s;
                """,
                    [user_id],
                )
                results = []
                for row in db.fetchall():
                    record = {}
                    for i, col in enumerate(db.description):
                        record[col.name] = row[i]
                    results.append(record)
                return results

    def get_task(self, task_id, user_id):
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                SELECT t.title, t.description, t.due_date, t.is_completed
                , t.project_id, t.id, t.created
                FROM tasks t
                JOIN projects p
                ON t.project_id = p.id
                WHERE t.id = %s AND p.user_id = %s;
                """,
                    [task_id, user_id],
                )
                record = None
                row = db.fetchone()
                if row is not None:
                    record = {}
                    for i, col in enumerate(db.description):
                        record[col.name] = row[i]
                return record

    def delete_task(self, task_id, user_id):
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                DELETE FROM tasks t
                USING projects p
                WHERE t.project_id = p.id
                AND t.id = %s
                AND p.user_id = %s;
                """,
                    [task_id, user_id],
                )
                if db.rowcount != 1:
                    return None
                return True

    def update_task(self, task_id, user_id, info):
        with pool.connection() as conn:
            with conn.cursor() as db:
                parems = [
                    info.title,
                    info.description,
                    info.due_date,
                    info.is_completed,
                    info.project_id,
                    task_id,
                    user_id
                ]
                results = db.execute(
                    """
                    UPDATE tasks t
                    SET title = %s, description = %s,
                    due_date = %s, is_completed = %s,
                    project_id = %s
                    FROM projects p
                    WHERE  t.project_id=p.id AND t.id = %s AND p.user_id=%s
                    """,
                    parems
                )
                print("-------resultys---=-----", results)
                updated_record = self.get_task(task_id, user_id)
                return updated_record
