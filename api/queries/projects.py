from queries.pool import pool
from models.projects import ProjectsOut, ProjectsIn


class ProjectsRepo:
    def create(self, projects_in: ProjectsIn, user_id):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                INSERT INTO projects
                    (title, description, due_date, user_id)
                VALUES
                (%s, %s, %s, %s)
                RETURNING id, created;
                """,
                    [
                        projects_in.title,
                        projects_in.description,
                        projects_in.due_date,
                        user_id,
                    ],
                )
                results = result.fetchone()
                id = results[0]
                created = results[1]
                old_data = projects_in.dict()
                project_out = ProjectsOut(id=id, created=created, **old_data)
                return project_out

    def get_all_projects(self, user_id):
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                SELECT * FROM projects
                WHERE user_id = %s;
                """,
                    [user_id],
                )
                results = []
                for row in db.fetchall():
                    record = {}
                    for i, col in enumerate(db.description):
                        record[col.name] = row[i]
                    results.append(record)
                return results

    def get_project(self, project_id, user_id):
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                SELECT *
                FROM projects
                WHERE id = %s AND user_id = %s;
                """,
                    [project_id, user_id],
                )
                record = None
                row = db.fetchone()
                if row is not None:
                    record = {}
                    for i, col in enumerate(db.description):
                        record[col.name] = row[i]
                return record

    def delete_project(sef, project_id, user_id):
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM projects
                    WHERE id = %s AND user_id = %s;
                    """,
                    [project_id, user_id],
                )
            if db.rowcount != 1:
                return None
            return True

    def update_project(self, project_id, user_id, info):
        with pool.connection() as conn:
            with conn.cursor() as db:
                parems = [
                    info.title,
                    info.description,
                    info.due_date,
                    project_id,
                    int(user_id),
                ]
                db.execute(
                    """
                    UPDATE projects SET title = %s, description = %s, due_date = %s
                    WHERE id = %s AND user_id = %s
                    RETURNING id, title, description, created;
                    """,
                    parems,
                ),
                record = None
                row = db.fetchone()
                if row is not None:
                    record = {}
                    for i, col in enumerate(db.description):
                        record[col.name] = row[i]
                return record

    def get_project_with_tasks(self, project_id, user_id):
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT * FROM tasks
                    WHERE tasks.project_id = %s;
                    """,
                    [project_id],
                ),
                project = self.get_project(project_id, user_id)
                tasks = []
                for row in db.fetchall():
                    record = {}
                    for i, col in enumerate(db.description):
                        record[col.name] = row[i]
                    tasks.append(record)
                project["tasks"] = tasks
                return project
