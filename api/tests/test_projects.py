from fastapi.testclient import TestClient
from main import app
from queries.projects import ProjectsRepo
from authenticator import authenticator

client = TestClient(app)


def fake_get_current_account_data():
    return {"id": 1, "username": "string"}


class FakeProjectQueries:
    def get_all_projects(self, user_id=1):
        return [{"title": "youtube", "description": "", "due_date": None, "id": 22, "created": "2023-09-06"}]

    def get_projects(self, project_id=1, user_id=1):
        return []

    def get_project(self, project_id=17, user_id=1):
        return {
            "title": "string",
            "description": "string",
            "due_date": "2023-09-11",
            "id": 17,
            "created": "2023-09-11"
        }


def test_get_all_my_projects():
    app.dependency_overrides[authenticator.get_current_account_data] = fake_get_current_account_data
    app.dependency_overrides[ProjectsRepo] = FakeProjectQueries
    response = client.get("/api/projects/mine")
    data = response.json()
    assert response.status_code == 200
    assert data == {"projects": [{"title": "youtube", "description": "", "due_date": None, "id": 22, "created": "2023-09-06"}]}


def test_get_project():
    app.dependency_overrides[authenticator.get_current_account_data] = fake_get_current_account_data
    app.dependency_overrides[ProjectsRepo] = FakeProjectQueries
    response = client.get("/api/projects/17")
    data = response.json()
    assert response.status_code == 200
    assert data == {
                "title": "string",
                "description": "string",
                "due_date": "2023-09-11",
                "id": 17,
                "created": "2023-09-11"
            }
