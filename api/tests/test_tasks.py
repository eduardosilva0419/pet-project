from fastapi.testclient import TestClient
from main import app
from queries.tasks import TasksRepo
from authenticator import authenticator

client = TestClient(app)


def get_current_account_data():
    return {"id": 1, "username": "paul"}


class FakeTaskQueries:
    def get_all_tasks(self, id=1):
        return [
            {
                "title": "Strum Learning",
                "description": "Read Book",
                "due_date": "2023-08-18T17:07:31.959000",
                "is_completed": True,
                "project_id": 1,
                "id": 2,
                "created": "2023-08-18",
            }
        ]

    def get_task(self, task_id=1, user_id=1):
        return {
            "title": "task1",
            "description": "string",
            "due_date": "2023-08-24T22:16:26.954000",
            "is_completed": True,
            "project_id": 2,
            "id": 1,
            "created": "2023-08-24",
        }


def test_get_all_tasks():
    app.dependency_overrides[authenticator.get_current_account_data] = get_current_account_data
    app.dependency_overrides[TasksRepo] = FakeTaskQueries

    res = client.get("api/tasks/mine")
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "tasks": [
            {
                "title": "Strum Learning",
                "description": "Read Book",
                "due_date": "2023-08-18T17:07:31.959000",
                "is_completed": True,
                "project_id": 1,
                "id": 2,
                "created": "2023-08-18",
            }
        ]
    }


def test_get_my_tasks():
    app.dependency_overrides[TasksRepo] = FakeTaskQueries
    app.dependency_overrides[authenticator.get_current_account_data] = get_current_account_data
    response = client.get("/api/tasks/1/")
    data = response.json()

    assert response.status_code == 200
    assert data == {
        "title": "task1",
        "description": "string",
        "due_date": "2023-08-24T22:16:26.954000",
        "is_completed": True,
        "project_id": 2,
        "id": 1,
        "created": "2023-08-24",
    }
