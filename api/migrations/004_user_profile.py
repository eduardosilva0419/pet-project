steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE profile (
            id SERIAL PRIMARY KEY NOT NULL,
            phone_number INTEGER NULL,
            email VARCHAR(256) NULL,
            address TEXT NULL,
            city TEXT NULL,
            state TEXT NULL,
            zip_code INTEGER NULL,
            user_id INTEGER NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE profile;
        """,
    ]
]
