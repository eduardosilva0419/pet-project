steps = [
    [
        # "Up" SQL statement
        """
        ALTER TABLE projects add column due_date date null;
        """,
        # "Down" SQL statement
        """
        DROP TABLE projects;
        """,
    ]
]
