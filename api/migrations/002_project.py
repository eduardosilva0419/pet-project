steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE projects (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR(100) NOT NULL,
            description TEXT NULL,
            created DATE DEFAULT CURRENT_TIMESTAMP,
            user_id INTEGER NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE projects;
        """,
    ]
]
