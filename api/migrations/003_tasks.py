steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE tasks (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR(100) NOT NULL,
            description TEXT NULL,
            created DATE DEFAULT CURRENT_TIMESTAMP,
            due_date TIMESTAMP NULL,
            is_completed BOOLEAN DEFAULT FALSE,
            project_id INTEGER NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE tasks;
        """,
    ]
]
