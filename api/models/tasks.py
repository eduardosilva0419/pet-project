from pydantic import BaseModel
from typing import Optional, List
from datetime import datetime, date


class TasksIn(BaseModel):
    title: str
    description: str
    due_date: Optional[datetime] = None
    is_completed: bool
    project_id: int


class TasksOut(TasksIn):
    id: int
    created: date


class AllTasksOut(BaseModel):
    tasks: List[TasksOut]
