from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token


class AccountForm(BaseModel):
    username: str
    password: str


class AccountOut(BaseModel):
    id: str
    username: str


class AccountOutWithHashedPassword(AccountOut):
    hashed_password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


class Account(BaseModel):
    username: str
    hashed_password: str


class AccountIn(BaseModel):
    username: str
    password: str


# Need to find correct way to do this for Router try/except statement
class DuplicateAccountError(Exception):
    username: str
