from pydantic import BaseModel
from typing import Optional, List
from datetime import date
from .tasks import TasksOut


class ProjectsIn(BaseModel):
    title: str
    description: Optional[str] = None
    due_date: Optional[date] = None


class ProjectsOut(ProjectsIn):
    id: int
    created: date


class AllProjectsOut(BaseModel):
    projects: List[ProjectsOut]


class ProjectId(int):
    id: int


class ProjectsUpdateIn(ProjectsIn):
    is_completed: bool


class ProjectsAndTasks(ProjectsOut):
    tasks: List[TasksOut]
