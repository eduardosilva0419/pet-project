import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { App, store} from "components";
import { Provider } from "react-redux";
import { CheckLogin } from "components/checkLogin";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
	<React.StrictMode>
		<Provider store={store}>
			<CheckLogin />
		</Provider>
	</React.StrictMode>
);
