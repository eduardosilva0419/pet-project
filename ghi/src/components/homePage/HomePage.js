import "pure-react-carousel/dist/react-carousel.es.css";
import { useSelector } from "react-redux";
import {
  CardCarousel,
  CreateProject,
  WeekView,
} from "components";


export const HomePage = () => {
  const showProject = useSelector((state) => state.toggleProjectPage.value);
  return (
    <>
      <div className="flex py-8">
        <p className="text-center flex-1 text-3xl font-bold">
          Projects
        </p>
      </div>
      <div>{showProject && <CreateProject />}</div>
      <div>
        <CardCarousel />
      </div>

      <div className="">
        <WeekView />
      </div>
    </>
  );
};
