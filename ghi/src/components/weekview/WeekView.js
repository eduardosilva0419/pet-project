import { useState, useEffect } from "react";
import { useGetAllTasksForUserQuery } from "components/queries";
import { useSelector, useDispatch } from "react-redux";
import formatDate from "components/helpers/formatDate";
import { TaskDetails } from "components/tasks";
import { selectedTaskId } from "components/store/taskSlice";

const listWeekDaysByName = () => {
  const daysOfWeek = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];

  let dayOfWeekList = [];
  const todaysDate = new Date();
  for (let i = 0; i < 7; i++) {
    let nextDate = new Date(todaysDate);
    nextDate.setDate(todaysDate.getDate() + i);
    dayOfWeekList.push(nextDate);
  }

  let orderedWeekDayObject = {};

  for (let day of dayOfWeekList) {
    const dayName = daysOfWeek[day.getDay()];
    const formattedDate = formatDate(day);
    orderedWeekDayObject[dayName] = formattedDate;
  }
  return orderedWeekDayObject;
};

export const WeekView = () => {
  const selectedTask = useSelector((state) => state.selectedTaskId.id);
  const dispatch = useDispatch();
  const weekDayList = listWeekDaysByName();
  const { data: tasks, isLoading } = useGetAllTasksForUserQuery();
  const [userTasks, setUserTasks] = useState([]);

  useEffect(() => {
    if (tasks && tasks.tasks) {
      setUserTasks(tasks.tasks);
    }
  }, [isLoading, tasks]);

  if (!userTasks || isLoading) {
    return <h1>Loading...</h1>;
  }

  return (
    <>
      <div className="container bg-blue-300 py-8 border rounded-2xl">
        <h1 className="text-center text-5xl pb-8">
          Your Week<hr></hr>
        </h1>
        <div className="grid grid-cols-7">
          {Object.keys(weekDayList).map((weekday) => (
            <div key={weekday} className="p-2">
              <p className="pb-2 font-bold underline ">
                {weekday} {weekDayList[weekday]}
              </p>
              <ul>
                {userTasks.map((task) => {
                  if (formatDate(task.due_date) === weekDayList[weekday]) {
                    return (
                      <li
                        key={task.id}
                        className="cursor-pointer hover:underline hover:bg-white hover:bg-opacity-50"
                        onClick={() => dispatch(selectedTaskId(task.id))}
                      >
                        {task.title}
                      </li>
                    );
                  }
                  return null;
                })}
              </ul>
            </div>
          ))}
        </div>
      </div>
      <div className="grid grid-cols-3">
        <div className="content-none"></div>
        <div>
          {selectedTask ? (
            <>
              <div className="p-2 bg-red-300 rounded-lg m-3">
                <TaskDetails />
              </div>
            </>
          ) : null}
        </div>
        <div className="content-none"></div>
      </div>
    </>
  );
};
