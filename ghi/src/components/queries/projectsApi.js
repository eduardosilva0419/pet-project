import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const projectsApi = createApi({
  reducerPath: "projectsApi",
  baseQuery: fetchBaseQuery({ baseUrl: process.env.REACT_APP_API_HOST }),
  tagTypes: ["account", "projects", "tasks"],
  endpoints: (builder) => ({
    createAccount: builder.mutation({
      query: (data) => {
        return {
          url: "/api/accounts",
          method: "POST",
          body: data,
          credentials: "include",
        };
      },
      invalidatesTags: ["account"],
    }),
    deleteProject: builder.mutation({
      query: (project_id) => {
        console.log("project id in delete mutation", project_id);
        return {
          url: `/api/projects/${project_id}`,
          method: "DELETE",
          credentials: "include",
        };
      },
      invalidatesTags: ["projects", "tasks"],
    }),
    createProject: builder.mutation({
      query: (data) => {
        console.log("query data", data);
        return {
          url: "/api/projects/mine",
          method: "POST",
          body: data,
          credentials: "include",
        };
      },
      invalidatesTags: ["projects"],
    }),
    getTokenForUser: builder.query({
      query: () => {
        return { url: "/token", credentials: "include" };
      },
      providesTags: ["account"],
    }),
    getAllProjectsForUser: builder.query({
      query: () => {
        return { url: "/api/projects/mine", credentials: "include" };
      },
      providesTags: ["projects"],
    }),
    getOneProject: builder.query({
      query: (project_id) => {
        return { url: `/api/projects/${project_id}`, credentials: "include" };
      },
      providesTags: ["projects"],
    }),
    getAllTasksForUser: builder.query({
      query: () => {
        return { url: "/api/tasks/mine", credentials: "include" };
      },
      providesTags: ["tasks", "projects"],
    }),
    getProjectAndTasks: builder.query({
      query: (projectId) => {
        return {
          url: `/api/projects/${projectId}/tasks`,
          credentials: "include",
        };
      },
      providesTags: ["projects", "tasks"],
    }),
    Login: builder.mutation({
      query: (info) => {
        const formData = new FormData();
        formData.append("username", info.username);
        formData.append("password", info.password);
        return {
          url: "/token",
          method: "POST",
          body: formData,
          credentials: "include",
        };
      },
      invalidatesTags: ["account"],
    }),
    Logout: builder.mutation({
      query: () => {
        return {
          url: "/token",
          method: "DELETE",
          credentials: "include",
        };
      },
      invalidatesTags: ["account"],
    }),
    updateProject: builder.mutation({
      query(data) {
        const id = data.formData.id;
        delete data.formData.id;
        const body = data.formData;
        return {
          url: `api/projects/${id}`,
          method: "PUT",
          body,
          credentials: "include",
        };
      },
      invalidatesTags: ["projects"],
    }),
    getTaskDetails: builder.query({
      query: (task_id) => {
        return {
          url: `/api/tasks/${task_id}`,
          credentials: "include",
        };
      },
    }),
    createTask: builder.mutation({
      query: (data) => {
        return {
          url: "/api/tasks",
          method: "POST",
          body: data,
          credentials: "include",
        };
      },
      invalidatesTags: ["tasks", "projects"],
    }),
    deleteTask: builder.mutation({
      query: (taskId) => {
        return {
          url: `/api/tasks/${taskId}`,
          method: "DELETE",
          credentials: "include",
        };
      },
      invalidatesTags: ["tasks"],
    }),
    updateTask: builder.mutation({
      query(data) {
        const id = data.taskId;
        const body = data.formData;
        console.log("id", id);
        console.log("body", body);
        return {
          url: `api/tasks/${id}`,
          method: "PUT",
          body,
          credentials: "include",
        };
      },
      invalidatesTags: ["tasks"],
    }),
  }),
});

export const {
  useGetTokenForUserQuery,
  useCreateAccountMutation,
  useGetAllProjectsForUserQuery,
  useGetOneProjectQuery,
  useGetAllTasksForUserQuery,
  useGetProjectAndTasksQuery,
  useLoginMutation,
  useLogoutMutation,
  useCreateProjectMutation,
  useDeleteProjectMutation,
  useUpdateProjectMutation,
  useGetTaskDetailsQuery,
  useCreateTaskMutation,
  useDeleteTaskMutation,
  useUpdateTaskMutation,
} = projectsApi;
