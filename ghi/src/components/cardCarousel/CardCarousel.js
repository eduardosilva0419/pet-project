import {
	CarouselProvider,
	Slider,
	Slide,
	ButtonBack,
	ButtonNext,
} from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";
import { useGetAllProjectsForUserQuery,ProjectCard } from "components";
import { toggleCreateProject } from "components/store/projectPageSlice";
import { useDispatch } from "react-redux";

export const CardCarousel = () => {
	const dispatch = useDispatch();
	const { data: projectsData, isLoading } = useGetAllProjectsForUserQuery();
	if (isLoading) {
		return <div>Projects Loading...</div>;
	}
	const project = projectsData.projects;
	let slideLen = project.length;
	if (project.length > 3) {
		slideLen = 3;
	}
	const handleAddProject = () => {
		dispatch(toggleCreateProject());
	};

	return (
		<div className="container w-full">
			<div className="items-center justify-center h-full py-24">
				<CarouselProvider
					className="lg:block hidden max-w-full"
					naturalSlideWidth={250}
					isIntrinsicHeight={true}
					totalSlides={project.length}
					visibleSlides={slideLen}
					step={1}
					infinite={true}
				>
					<div className="flex">
						<div className="min-w-full">
							<Slider>
								<div
									id="slider"
									className=" h-full max-w-full flex-1 overflow-hidden transition ease-out duration-700"
								>
									{project.map((proj, index) => {
										return (
											<Slide className="pl-4 pr-4 " index={index} key={index}>
												<ProjectCard props={proj} />
											</Slide>
										);
									})}
								</div>
							</Slider>
						</div>
					</div>
					<div className="grid grid-cols-3 mt-5">
						<div className="justify-self-start">
							<ButtonBack role="button" aria-label="slide backward" id="prev">
								<i className="fa-solid fa-arrow-left text-3xl  "></i>
							</ButtonBack>
						</div>
						<div className="justify-self-center">
							<button
								className="text-right bg-blue-500 hover text-white rounded-md p-2 ml-10 mx-center"
								onClick={handleAddProject}
							>
								Add Project{" "}
							</button>
						</div>
						<div className="justify-self-end">
							<ButtonNext role="button" aria-label="slide forward" id="next">
								<i className="fa-solid fa-arrow-right text-3xl item"></i>
							</ButtonNext>
						</div>
					</div>
				</CarouselProvider>
			</div>
		</div>
	);
};
