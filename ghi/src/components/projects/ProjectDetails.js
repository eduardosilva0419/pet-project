import { useEffect, useState } from "react";
import {
	useGetProjectAndTasksQuery,
	useDeleteProjectMutation,
} from "components";
import { TaskDetails, EditTask } from "components/tasks";
import { useDispatch } from "react-redux";
import { selectedTaskId } from "components/store/taskSlice";
import { useNavigate, useParams } from "react-router-dom";
import { EditProject, CreateTaskForm, TaskList } from "components";

export const ProjectDetails = () => {
	const [showCreateTaskForm, setShowCreateTaskForm] = useState(false);
	const navigate = useNavigate();
	let projectId = useParams().id;
	const [delProj, delProjResponse] = useDeleteProjectMutation();
	const [editOrDetailOrAdd, setEditOrDetail] = useState(0);

	useEffect(() => {
		if (delProjResponse.isSuccess) {
			navigate("/");
		}
		if (delProjResponse.isError) {
			console.log("error");
		}
	}, [delProjResponse, navigate]);
	const projDelete = (e) => {
		e.preventDefault();
		delProj(projectId);
	};

	const dispatch = useDispatch();

	const { data: project, isLoading } = useGetProjectAndTasksQuery(projectId);
	const [showEditProject, setShowEditProject] = useState(false);
	if (!projectId) {
		return <h1>No Project Selected</h1>;
	}
	if (isLoading) {
		return <p>loading...</p>;
	}

	const tasks = project.tasks;
	const handleEditProject = () => {
		setShowCreateTaskForm(false);
		setShowEditProject(!showEditProject);
	};

	const handleAddTask = (e) => {
		setEditOrDetail(3);
	};

	return (
		<div className="flex flex-wrap pt-6 h-full">
			<div className="w-full lg:w-1/2 pr-4">
				{" "}
				<div className="bg-white rounded-lg flex flex-col items-center justify-center px-6 py-8 lg:py-0 mt-10 ml-5 bg-white rounded-lg border rounded-lg">
					<h1 className="text-2xl font-semibold text-center rounded-lg p-4 mt-2">
						Project: {project.title}
					</h1>
					{project.description && (
						<div className="w-full text-center text-lg rounded-lg p-2">
							<i className="fa-regular fa-clipboard"></i> {project.description}
						</div>
					)}
					{project.due_date && (
						<p className="text-center pb-4">
							<i className="fa-regular fa-calendar"></i> {project.due_date}
						</p>
					)}
					<div className="flex self-center m-4">
						<button
							onClick={handleEditProject}
							className="bg-gray-200 hover:bg-gray-300 text-black font-bold py-2 px-4 rounded-md mx-4"
						>
							Edit
						</button>
						<button
							onClick={projDelete}
							className="bg-red-300 hover:bg-red-700 text-black font-bold py-2 px-4 rounded-md mx-4"
						>
							Delete
						</button>
					</div>
				</div>
				{showEditProject && <div><EditProject /> </div>}
			</div>
			<div className="w-full lg:w-1/2 pl-4">
				{" "}
				<div className="grid grid-cols-1 w-full mt-8 dark:bg-gray-800 dark:border-gray-700 text-black">
					<div className=" bg-blue-200 rounded-lg p-6 m-2">
						<i className="fa-solid fa-list-check"></i>{" "}
						<TaskList
							tasks={tasks}
							handleEditTask={(taskId) => {
								dispatch(selectedTaskId(taskId));
								setEditOrDetail(1);
							}}
							handleTaskDetails={(taskId) => {
								dispatch(selectedTaskId(taskId));
								setEditOrDetail(2);
							}}
						/>
						<div className="truncate">
							<button
								className="bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 mt-3 rounded-md"
								onClick={handleAddTask}
							>
								Add Task
							</button>
						</div>
					</div>
				</div>
				<div className="rounded-lg p-6 m-2 bg-red-300">
					{editOrDetailOrAdd === 1 ? (
						<EditTask />
					) : editOrDetailOrAdd === 2 ? (
						<TaskDetails />
					) : editOrDetailOrAdd === 3 ? (
						<CreateTaskForm props={projectId} />
					) : null}
				</div>
			</div>
		</div>
	);
};
