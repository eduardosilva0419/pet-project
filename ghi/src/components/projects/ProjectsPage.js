import { ProjectCard, CreateProject,useGetAllProjectsForUserQuery } from "components";
import { toggleCreateProject } from "components/store/projectPageSlice";
import { useSelector, useDispatch } from "react-redux";

export const ProjectsPage = () => {
	const { data: projectsData, isLoading } = useGetAllProjectsForUserQuery();
	const showProject = useSelector((state) => state.toggleProjectPage.value);
	const dispatch = useDispatch();

	if (isLoading) {
		return <div>Projects Loading...</div>;
	}
	const project = projectsData.projects;

	return (
		<div className="pt-10">
			<button
				className="button bg-blue-500 hover text-white rounded-md p-2 ml-10 mx-center"
				onClick={() => dispatch(toggleCreateProject())}
			>
				Add Project
			</button>
			{showProject && <CreateProject />}
			<div className="grid gap-4 grid-cols-3 p-10 justify-around">
				{project.map((proj) => {
					return <ProjectCard props={proj} />;
				})}
			</div>
		</div>
	);
};
