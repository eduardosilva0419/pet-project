import formatDate from "components/helpers/formatDate";


export const TaskList = ({ tasks, handleEditTask, handleTaskDetails }) => {
	return (
		<table className="w-full">
			<thead className="text-left">
				<tr>
					<th className="text-xl font-semibold">Task</th>
					<th className="text-xl font-semibold min-w-[100px]">Due Date</th>
					<th className="text-xl font-semibold min-w-[50px]">Actions</th>
				</tr>
			</thead>
			<tbody>
				{tasks &&
					tasks.map((task) => {
						return (
							<tr className="hover:bg-white hover:bg-opacity-25" key={task.id}>
								<td className="truncate" style={{ maxWidth: "250px" }}>
									{task.is_completed ? (
										<i className="fa-regular fa-circle-check"></i>
									) : null}{" "}
									{task.title}
								</td>
								<td>{formatDate(task.due_date)}</td>
								<td className="min-w-[100px]">
									<span
										className="cursor-pointer hover:underline"
										onClick={() => {
											handleEditTask(task.id);
										}}
									>
										Edit
									</span>
									<span> </span>
									<span
										className="mx-2 cursor-pointer hover:underline"
										onClick={() => {
											handleTaskDetails(task.id);
										}}
									>
										Details
									</span>
								</td>
							</tr>
						);
					})}
			</tbody>
		</table>
	);
};
