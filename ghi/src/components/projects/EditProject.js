import { useState, useEffect } from "react";
import {
	useUpdateProjectMutation,
	useGetOneProjectQuery,
} from "components/queries/projectsApi";
import { useSelector } from "react-redux";

export const EditProject = () => {
	const projectId = useSelector((state) => state.selectedProjectId.id);
	const { data: projectData, isLoading } = useGetOneProjectQuery(projectId);
	const [formData, setFormData] = useState({});
	const [updateProject, updateProjectResponse] = useUpdateProjectMutation();

	const handleFormChanges = (e) => {
		const { name, value } = e.target;
		setFormData({ ...formData, [name]: value });
	};

	useEffect(() => {
		if (projectData) {
			setFormData(projectData);
		}
	}, [isLoading, projectData]);
	if (!projectData) {
		return <h1>No project selected</h1>;
	}

	const handleSubmit = (e) => {
		e.preventDefault();
		delete formData.created;
		updateProject({ formData });
	};

	return (
		<div className="bg-gray-300 rounded-lg flex flex-col px-6 py-8 lg:py-0 mt-10 ml-5 bg-white rounded-lg shadow border rounded-lg">
			<div className="flex-1 mt-8">
				<h1 className="text-xl pb-2 font-semibold text-center rounded-lg">
					Update Project
				</h1>
				<hr className="pb-2"></hr>
			</div>
			<form className="justify-content-center flex flex-col items-center">
				<div className="md:flex mb-6">
					<div className="md:w-1/3">
						<label
							className="block text-black font-bold mb-1 md:mb-0 pr-4"
							htmlFor="projectName"
						>
							Title:
						</label>
					</div>
					<div>
						<input
							id="projectName"
							className=" border-2 border-gray-200 rounded-md w-50 py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-blue-500 overflow-ellipsis"
							type="text"
							value={formData.title}
							name="title"
							onChange={handleFormChanges}
						/>
					</div>
				</div>
				<div className="md:flex mb-6">
					<div className="md:w-1/3">
						<label
							className="block text-black font-bold mb-1 md:mb-0 pr-4"
							htmlFor="taskDescription"
						>
							Description:
						</label>
					</div>
					<div className="md:w-2/3">
						<textarea
							name="description"
							className="appearance-none border-2 border-gray-200 rounded-md w-50 py-8 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500 overflow-ellipsis"
							value={formData.description}
							onChange={handleFormChanges}
						/>
					</div>
				</div>
				<div className="md:flex md:items-center mb-6">
					<div className="md:w-1/3">
						<label className="block text-black font-bold " htmlFor="dueDate">
							Due Date:
						</label>
					</div>
					<div className="md:w-2/3">
						<input
							className="appearance-none border-2 border-gray-200 rounded-md w-50 py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
							type="date"
							value={formData.due_date}
							name="due_date"
							onChange={handleFormChanges}
						/>
					</div>
				</div>
				<div>
					<button
						type="submit"
						onClick={handleSubmit}
						className="bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 rounded-md my-4 mx-8"
					>
						Update
					</button>
				</div>
			</form>
		</div>
	);
};
