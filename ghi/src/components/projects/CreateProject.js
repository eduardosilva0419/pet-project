import { useState, useEffect } from "react";
import { useCreateProjectMutation } from "components/queries";
import { useDispatch } from "react-redux";
import { toggleCreateProject } from "components/store/projectPageSlice";


export const CreateProject = () => {
  const [formData, setFormData] = useState({});
  const [createProject, createProjectResponse] = useCreateProjectMutation("");
  const dispatch = useDispatch();

  const handleFormData = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  useEffect(() => {
    if (createProjectResponse.isSuccess) {
      dispatch(toggleCreateProject());
    }
    if (createProjectResponse.isError) {
      console.log("error");
    }
  }, [createProjectResponse, dispatch]);

  const handleSubmit = (e) => {
    e.preventDefault();
    createProject(formData);
  };

  return (
    <>
      <section className="dark:bg-transparent">
        <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto">
          <div className="bg-white rounded-lg shadow border-2">
            <div className="p-6 space-y-4 ">
              <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 dark:text-gray-900">
                <span>Create Project</span>
              </h1>

              <form onSubmit={handleSubmit} className="space-y-4 ">
                <div>
                  <label
                    htmlFor="Login__username"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-900"
                  >
                    Project Title
                  </label>
                  <input
                    onChange={handleFormData}
                    type="text"
                    id="Create_title"
                    className="bg-gray-50 border border-gray-300 text-gray-900 rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-900 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Title"
                    name="title"
                    required
                  />
                </div>
                <div>
                  <label
                    htmlFor="Login__password"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-900"
                  >
                    Project Description (optional)
                  </label>
                  <input
                    onChange={handleFormData}
                    type="description"
                    name="description"
                    id="Create_desc"
                    placeholder="Description"
                    className="bg-gray-50 border border-gray-300 text-gray-900  rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  />
                </div>
                <div>
                  <label
                    htmlFor="due_date"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-900"
                  >
                    Due Date (optional)
                  </label>
                  <input
                    onChange={handleFormData}
                    type="date"
                    name="due_date"
                    id="due_date"
                    placeholder="0/0/00"
                    className="bg-gray-50 border border-gray-300 text-gray-900  rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-300 dark:border-gray-600 dark:placeholder-gray-100 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  />
                </div>
                <button
                  type="submit"
                  className="button bg-blue-500 hover text-white rounded-md p-2 mx-center"
                >
                  Submit
                </button>
              </form>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
