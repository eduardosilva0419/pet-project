import { useState, useEffect } from "react";
import {
	useGetTaskDetailsQuery,
	useUpdateTaskMutation,
} from "components/queries/projectsApi";
import { useSelector } from "react-redux";

export const EditTask = () => {
	const taskId = useSelector((state) => state.selectedTaskId.id);
	const { data: taskData, isLoading } = useGetTaskDetailsQuery(taskId);
	const [formData, setFormData] = useState({});
	const [updateTask, updateTaskResponse] = useUpdateTaskMutation();

	const handleFormChanges = (e) => {
		const { name, value, type, checked } = e.target;
		let newValue = type === "checkbox" ? checked : value;
		setFormData({ ...formData, [name]: newValue });
	};

	useEffect(() => {
		if (taskData) {
			setFormData(taskData);
		}
	}, [isLoading, taskData, taskId]);

	const handleSubmit = (e) => {
		e.preventDefault();
		delete formData.id;
		delete formData.created;
		updateTask({ taskId, formData });
	};

	if (!taskId) {
		return <h1>No task selected</h1>;
	}
	if (isLoading) {
		return <h1>Loading</h1>;
	}

	return (
		<form>
			<h1 className="flex-1 text-xl pb-2 font-semibold text-center rounded-lg">
				Edit Task
			</h1>
			<hr className="pb-2"></hr>
			<div className="md:flex mb-6">
				<div className="md:w-1/3">
					<label
						className="block text-black font-bold mb-1 md:mb-0 pr-4"
						htmlFor="taskName"
					>
						Task:
					</label>
				</div>
				<div>
					<input
						className="border-2 border-gray-200 rounded-md w-50 py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-blue-500 overflow-ellipsis"
						type="text"
						value={formData.title}
						name="title"
						onChange={handleFormChanges}
					/>
				</div>
			</div>
			<div className="md:flex mb-6">
				<div className="md:w-1/3">
					<label
						className="block text-black font-bold mb-1 md:mb-0 pr-4"
						htmlFor="taskDescription"
					>
						Description:
					</label>
				</div>
				<div className="md:w-2/3">
					<textarea
						name="description"
						className="appearance-none border-2 border-gray-200 rounded-md w-50 py-8 px-4 text-black leading-tight focus:outline-none focus:bg-white focus:border-blue-500 overflow-ellipsis"
						value={formData.description}
						onChange={handleFormChanges}
					/>
				</div>
			</div>
			<div className="md:flex md:items-center mb-6">
				<div className="md:w-1/3">
					<label className="block text-black font-bold " htmlFor="dueDate">
						Due Date:
					</label>
				</div>
				<div className="md:w-2/3">
					<input
						className="appearance-none border-2 border-gray-200 rounded-md w-50 py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
						type="datetime-local"
						value={formData.due_date}
						name="due_date"
						onChange={handleFormChanges}
					/>
				</div>
			</div>
			<div>
				<label htmlFor="is_completed">Completed: </label>
				<input
					className="text-black m-3 font-bold"
					id="is_completed"
					type="checkbox"
					value={formData.is_completed}
					name="is_completed"
					onChange={handleFormChanges}
				/>
			</div>
			<div>
				<button
					className="bg-red-500 hover:bg-red-400 text-white font-bold py-2 px-4 rounded-md p-2"
					type="submit"
					onClick={handleSubmit}
				>
					Update
				</button>
			</div>
		</form>
	);
};
