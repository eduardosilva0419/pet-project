import { useDeleteTaskMutation } from "components/queries/projectsApi";

export const DeleteTask = ({ id }) => {
  const [deleteTask, deleteTaskStatus] = useDeleteTaskMutation();
  const handleDelete = async (taskId) => {
    try {
      const result = await deleteTask(taskId);
      if (result.error) {
        console.error("Failed to delete", result.error);
      } else {
      }
    } catch (error) {
      console.error("Error occurred", error);
    }
  };
  return (
    <div className="md:flex mb-6">
      <div className="md:w-1/3">
        <label className="block text-black font-bold mb-1 md:mb-0 pr-4">
          Delete Task
        </label>
      </div>
      <div>
        <i
          className="fa-regular fa-trash-can cursor-pointer cursor-pointer"
          onClick={() => handleDelete(id)}
        ></i>
      </div>
    </div>
  );
};
