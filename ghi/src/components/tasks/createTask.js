import { useState } from "react";
import { useCreateTaskMutation } from "components/queries/projectsApi";


export const CreateTaskForm = (projectId) => {
  const [taskName, setTaskName] = useState("");
  const [taskDescription, setTaskDescription] = useState("");
  const [taskDueDate, setDueDate] = useState(null | "");
  const [taskIsCompleted, setIsCompleted] = useState(false);
  const [taskProjectId, setProjectId] = useState("");
  const [createTask, createTaskResponse] = useCreateTaskMutation();

  const handleSubmit = (e) => {
    e.preventDefault();

    const formData = {};

    formData.title = taskName;
    formData.description = taskDescription;
    formData.due_date = taskDueDate;
    formData.is_completed = taskIsCompleted;
    formData.project_id = projectId.props;
    createTask(formData);

    setTaskName("");
    setTaskDescription("");
    setDueDate("");
    setIsCompleted(false);
    setProjectId("");
  };

  return (
    <div className="rounded-2xl">
      <h1 className="flex-1 text-xl pb-2 font-semibold text-center rounded-lg">
        Create New Task
      </h1>
      <hr className="pb-2"></hr>
      <form onSubmit={handleSubmit}>
        <div className="md:flex mb-6">
          <div className="md:w-1/3">
            <label
              className="block text-black font-bold mb-1 md:mb-0 pr-4"
              htmlFor="taskName"
            >
              Task:
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              className=" border-2 border-gray-200 rounded-md w-50 py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-blue-500 overflow-ellipsis"
              type="text"
              id="taskName"
              value={taskName}
              onChange={(e) => setTaskName(e.target.value)}
            />
          </div>
        </div>
        <div className="md:flex mb-6">
          <div className="md:w-1/3">
            <label
              className="block text-black font-bold mb-1 md:mb-0 pr-4"
              htmlFor="taskDescription"
            >
              Description:
            </label>
          </div>
          <div className="md:w-2/3">
            <textarea
              className="appearance-none border-2 border-gray-200 rounded-md w-50 py-8 px-4 text-black leading-tight focus:outline-none focus:bg-white focus:border-blue-500 overflow-ellipsis"
              id="taskDescription"
              value={taskDescription}
              onChange={(e) => setTaskDescription(e.target.value)}
            />
          </div>
        </div>
        <div className="md:flex md:items-center mb-6">
          <div className="md:w-1/3">
            <label className="block text-black font-bold " htmlFor="dueDate">
              Due Date:
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              className="appearance-none border-2 border-gray-200 rounded-md w-50 py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              type="datetime-local"
              id="dueDate"
              value={taskDueDate}
              onChange={(e) => setDueDate(e.target.value)}
            />
          </div>
        </div>
        <div className="md:w-1/3 py-5">
          <button
            className="bg-red-500 hover:bg-red-400 text-white font-bold py-2 px-4 rounded-md p-2"
            type="submit"
          >
            Create Task
          </button>
        </div>
      </form>
    </div>
  );
};
