import { useGetTaskDetailsQuery } from "components/queries/projectsApi";
import { useState, useEffect } from "react";
import formatDate from "components/helpers/formatDate";
import formatTime from "components/helpers/formatTime";
import { useSelector } from "react-redux";
import { DeleteTask } from "./DeleteTask";

export const TaskDetails = () => {
  const taskId = useSelector((state) => state.selectedTaskId.id);
  const { data: task, isLoading } = useGetTaskDetailsQuery(taskId);
  const [taskData, setTaskData] = useState({});

  useEffect(() => {
    if (task) {
      setTaskData(task);
    }
  }, [taskId, task]);

  if (!taskId) {
    return <h1>No Task Selected</h1>;
  }
  if (isLoading) {
    return <p>Loading...</p>;
  }

  const formattedDate = formatDate(taskData.due_date);
  const formattedTime = formatTime(taskData.due_date);

  return (
    <div className="flex flex-wrap h-full">
      <div className="flex flex-col w-full rounded-lg ">
        <h1 className="flex-1 text-xl pb-2 font-semibold text-center rounded-lg">
          {taskData.title}
        </h1>
        <hr className="pb-2"></hr>
        <div className="md:flex md:items-center mb-6">
          <div className="md:w-1/3">
            <label className="block text-black font-bold ">Due Date</label>
          </div>
          <div className="md:w-2/3">
            <p>
              {formattedTime} on {formattedDate}
            </p>
          </div>
        </div>
        <div className="md:flex mb-6">
          <div className="md:w-1/3">
            <label
              className="block text-black font-bold mb-1 md:mb-0 pr-4 "
              htmlFor="taskDescription"
            >
              Description:
            </label>
          </div>
          <div className="md:w-2/3 w-50">
            <p id="taskDescription"> {taskData.description}</p>
          </div>
        </div>
        <div className="md:flex md:items-center mb-6">
          <div className="md:w-1/3">
            <label className="block text-black font-bold ">Status</label>
          </div>
          <div className="md:w-2/3">
            <p className="w-full">
              {task.is_completed ? "Completed" : "Not Completed"}
            </p>
          </div>
        </div>
        <DeleteTask id={taskId} />
      </div>
    </div>
  );
};
