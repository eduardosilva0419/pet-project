import { useNavigate } from "react-router-dom";

export const LandingPage = () => {
  const navigate = useNavigate();

  return (
    <div
      className="flex flex-col items-center p-12 bg-opacity-50"
    >
      <img
        className="rounded-lg shadow-md w-full object-cover max-w-lg mb-8"
        src="https://images.pexels.com/photos/2144292/pexels-photo-2144292.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
        alt="Dog LandingPage"
      />
      <div className="text-center mb-8 bg-white bg-opacity-75 rounded p-4">
        <p className="text-2xl font-semibold mb-4">
          Your Daily Tasks With Your Best Buddy!
        </p>
      </div>
      <button
        onClick={() => navigate("/signup")}
        className="bg-blue-500 hover drop-shadow-md text-white rounded-md p-2 mx-auto mx-center"
      >
        Sign Up Now!
      </button>
    </div>
  );
};
