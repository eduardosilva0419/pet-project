import { BrowserRouter, Routes, Route } from "react-router-dom";
import {
	HomePage,
	Navbar,
	LandingPage,
	SignUp,
	ProjectDetails,
	LoginForm,
	ProjectsPage,
	LoadingWheel,
} from "components";
import { useSelector } from "react-redux";


export const App = () => {
	const userLoggedIn = useSelector((state) => state.loggedIn.value);
	const URL =
		"https://img.freepik.com/free-vector/cute-playful-footprint-pattern-background-fauna-fun_1017-44713.jpg?w=1380&t=st=1694193772~exp=1694194372~hmac=bb947f09c7fa8998c571b6303a477f6baf366eccf1afd3aa84e2f7409b117869";

	const backgroundImageOpacity = 0.05;

	return (
		<>
			<BrowserRouter>
				<div
					style={{
						position: "relative",
					}}
					className="min-h-screen m-w-screen"
				>
					<img
						src={URL}
						alt="Background"
						style={{
							opacity: backgroundImageOpacity,
							position: "absolute",
							width: "100%",
							height: "100%",
							objectFit: "cover",
							top: 0,
							left: 0,
							zIndex: -1,
						}}
					/>
					<Navbar />
					<div className="container mx-auto min-h-full ">
						<Routes>
							<Route
								path="/"
								element={userLoggedIn ? <HomePage /> : <LandingPage />}
							/>
							<Route path="/signup" element={<SignUp />} />
							<Route path="/project-detail/:id" element={<ProjectDetails />} />
							<Route path="/projects" element={<ProjectsPage />} />
							<Route path="/login" element={<LoginForm />} />
							<Route path="/loading" element={<LoadingWheel />} />
						</Routes>
					</div>
				</div>
			</BrowserRouter>
		</>
	);
};
