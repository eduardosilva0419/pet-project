import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import accountSlice from "./accountSlice";
import { projectsApi } from "components/queries/projectsApi";
import projectSlice from "./projectSlice";
import taskSlice from "./taskSlice";
import projectPageSlice from "./projectPageSlice";

export const store = configureStore({
  reducer: {
    loggedIn: accountSlice,
    selectedProjectId: projectSlice,
    selectedTaskId: taskSlice,
    toggleProjectPage: projectPageSlice,
    [projectsApi.reducerPath]: projectsApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(projectsApi.middleware),
});

setupListeners(store.dispatch);
