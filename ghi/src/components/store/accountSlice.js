import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  value: false,
};

export const accountSlice = createSlice({
  name: "accountLoggedIn",
  initialState: initialState,
  reducers: {
    loggedIn: (state, action) => {
      state.value = action.payload;
    },
  },
});

export let { loggedIn } = accountSlice.actions;

export default accountSlice.reducer;
