import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	id: null,
};

export const projectSlice = createSlice({
	name: "projectId",
	initialState,
	reducers: {
		selectedProjectId: (state, action) => {
			state.id = action.payload;
		},
	},
});

export const { selectedProjectId } = projectSlice.actions;

export default projectSlice.reducer;
