import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	id: null,
};

export const taskSlice = createSlice({
	name: "taskId",
	initialState,
	reducers: {
		selectedTaskId: (state, action) => {
			state.id = action.payload;
		},
		deleteTask: (state, action) => {
			const id = action.payload;
			const index = state.list.findIndex((task) => task.id === id);
			if (index !== -1) {
				state.list.splice(index, 1);
			}
		},
	},
});

export const { selectedTaskId, deleteTask } = taskSlice.actions;

export default taskSlice.reducer;
