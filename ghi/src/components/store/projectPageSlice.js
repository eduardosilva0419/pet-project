import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	value: false,
};

export const projectPageSlice = createSlice({
    name: "projectPage",
    initialState,
    reducers: {
        toggleCreateProject: (state) => {
            state.value = !state.value;
        },
    },
});

export const { toggleCreateProject } = projectPageSlice.actions;

export default projectPageSlice.reducer;
