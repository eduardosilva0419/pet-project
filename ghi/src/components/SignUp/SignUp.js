import { useState, useEffect } from "react";
import { useCreateAccountMutation } from "components/queries";
import { loggedIn } from "components/store";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";


export const SignUp = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [createAccount, createAccountResponse] = useCreateAccountMutation("");
  const [errorMessage, setErrorMessage] = useState("");

  function handleUsernameChange(event) {
    const value = event.target.value;
    setUsername(value);
  }

  function handlePasswordChange(event) {
    const value = event.target.value;
    setPassword(value);
  }

  function handleConfirmPassword(event) {
    const value = event.target.value;
    setConfirmPassword(value);
  }

  useEffect(() => {
    if (createAccountResponse.isSuccess) {
      dispatch(loggedIn(true));
      navigate("/");
    }
    if (createAccountResponse.isError) {
      setUsername("");
      setPassword("");
      setConfirmPassword("");
      setErrorMessage(createAccountResponse.error.data.detail);
      console.log(errorMessage)
    }
  }, [createAccountResponse, dispatch, errorMessage, navigate]);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (password !== confirmPassword) {
      setUsername("");
      setPassword("");
      setConfirmPassword("");
    } else {
      createAccount({ username, password });
    }
  };

  return (
    <div className="bg-opacity-100 rounded-lg flex flex-col items-center justify-center px-6 py-8 lg:py-0 mt-10">
      <div className="w-full bg-white rounded-lg md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
        <div className="p-6 space-y-4 shadow border rounded-lg md:space-y-6 sm:p-8">
          <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
            <span>Create an account</span>
          </h1>

          <form onSubmit={handleSubmit} className="space-y-4 md:space-y-6">
            <div>
              <label
                htmlFor="username"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Username
              </label>
              <input
                onChange={handleUsernameChange}
                type="text"
                name="username"
                id="username"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="username"
                required
              />
            </div>
            <div>
              <label
                htmlFor="password"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Password
              </label>
              <input
                onChange={handlePasswordChange}
                type="password"
                name="password"
                id="password"
                placeholder="••••••••"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                required
              />
            </div>
            <div>
              <label
                htmlFor="confirm-password"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Confirm password
              </label>
              <input
                onChange={handleConfirmPassword}
                type="password"
                name="confirm-password"
                id="confirm-password"
                placeholder="••••••••"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                required
              />
            </div>

            <button
              type="submit"
              className="bg-blue-500 hover text-white rounded-md p-2"
            >
              Create an account
            </button>
            <p className="text-sm font-light text-gray-500 dark:text-gray-400">
              Already have an account?{" "}
              <span
                onClick={() => navigate("/login")}
                className="cursor-pointer font-medium text-primary-600 hover:underline dark:text-primary-500"
              >
                Login here
              </span>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
};
