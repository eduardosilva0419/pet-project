import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { selectedProjectId } from "components";

export const ProjectCard = (props) => {

    const dispatch = useDispatch();

	if (!props) {
		return <h1>Loading</h1>;
	}

	return (
		<>
			<Link
				to={`/project-detail/${props.props.id}`}
				className="block max-w-sm p-8 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700"
				onClick={() => dispatch(selectedProjectId(props.props.id))}
			>
				<h5 className="mb-2 text-2xl font-bold tracking-tight line-clamp-1 text-gray-900 dark:text-white ">
					{props.props.title}
				</h5>
				<p className="font-normal line-clamp-1 text-gray-700 dark:text-gray-400">
					{props.props.description}
				</p>
			</Link>
		</>
	);
};
