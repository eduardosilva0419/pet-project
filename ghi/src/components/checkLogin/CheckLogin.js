import { App, useGetTokenForUserQuery,LoadingWheel, loggedIn } from "components"
import { useDispatch } from "react-redux"
export const CheckLogin = () =>{
    const dispatch = useDispatch();
    const {data: tokenData, isLoading} = useGetTokenForUserQuery()
    if (isLoading) {
        return <LoadingWheel />
    }
    if (tokenData != null) {
        dispatch(loggedIn(true))
    return (<App/>)}

    return <App />

}
