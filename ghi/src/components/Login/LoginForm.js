import { useState } from "react";
import { useLoginMutation } from "components/queries";
import { useNavigate } from "react-router-dom";
import { loggedIn } from "components/store";
import { useEffect } from "react";
import { useDispatch } from "react-redux";

export const LoginForm = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [login, loginResponse] = useLoginMutation("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");


  useEffect(() => {
    if (loginResponse.isSuccess) {
      dispatch(loggedIn(true));
      navigate("/");
    }
    if (loginResponse.isError) {
      setErrorMessage(loginResponse.error.data.detail);
    }
  }, [loginResponse, dispatch, navigate]);

  const handleSubmit = (e) => {
    e.preventDefault();
    login({ username, password });
  };

  return (
    <section className="bg-opacity-100 rounded-lg flex flex-col items-center justify-center px-6 py-8 lg:py-0 mt-10">
      <div className="w-full bg-white rounded-lg md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
        <div className="p-6 space-y-4 shadow border rounded-lg md:space-y-6 sm:p-8">
          <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
            <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
              <span>Login</span>
            </h1>

            <form onSubmit={handleSubmit} className="space-y-4 md:space-y-6">
              <div>
                <label
                  htmlFor="Login__username"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                >
                  Username
                </label>
                <input
                  onChange={(e) => setUsername(e.target.value)}
                  type="text"
                  id="Login__username"
                  className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="username"
                  value={username}
                  required
                />
              </div>
              <div>
                <label
                  htmlFor="Login__password"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                >
                  Password
                </label>
                <input
                  onChange={(e) => setPassword(e.target.value)}
                  type="password"
                  name="password"
                  id="Login__password"
                  placeholder="••••••••"
                  className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  required
                />
              </div>
              {errorMessage && (
                <h1 className="dark:text-white">{errorMessage}</h1>
              )}
              <button
                type="submit"
                className="button bg-blue-500 hover text-white rounded-md p-2"
              >
                Submit
              </button>
              <p className="text-sm font-light text-gray-500 dark:text-gray-400">
                Don't have an account?{" "}
                <span
                  onClick={() => navigate("/signup")}
                  className="cursor-pointer font-medium text-primary-600 hover:underline dark:text-primary-500"
                >
                  Sign up here
                </span>
              </p>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
};
