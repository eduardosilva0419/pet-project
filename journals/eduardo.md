## Mon, Aug 14, 2023

Today, I worked on:

- Setting up Tailwind and Authentication

I collaborated with Paul, Joe, and Julio to make progress on our Python project. We tackled the Docker-Compose.yaml setup and set up the visual aspect of our project using Tailwind CSS. Additionally, we organized our project structure with new directories and files to integrate PostgreSQL smoothly.

## Tue, Aug 15, 2023

Today, I worked on :

- Troubleshooting authentication backend login and logout functionality.

As a team we collaborated in reading documentation and implement changes to Authenticator.py , added GetAccountWithHashPassword function. Under get_hash_password we added the return statement and changed it to acccount["username"]. Under get_account_data_for_cookie
we added return statement with account["username"]. Also under authenticator.py we added def get_hashed_password(self,account: AccountOutWithHashedPassword,). Tested functionality and confirmed we are getting the token when logged in.

## Wed, Aug 16, 2023

Today I worked on "create project" front end, as well as adding a user ID to projects table in SQL backend.

## Thur, Aug 17, 2023

Today I worked on Delete and Update Projects.

## Fri, Aug 18, 2023

Today I worked on Task Update and Delete components and functionality integration to the backend in Models. Create profile models and merged it into main.

## Monday, Aug 21, 2023

Today, I worked on:

Developed front-end navigation bar for application. Included CSS from tailwind for beautification, and installed dependencies in file structure to ensure ease of code readability. Utilized component dependency method, which provided for cleaner code. This allows for export all components without the need for individual imports in the app.js file.

## Tue, Aug 22, 2023

Today I worked on Navbar functionality and CSS which was complete. I also worked on 22-redux issue in collaboration with paul.

## Wed, Aug 23, 2023

Today I worked with the team on landing page functionality, most specifically with paul. He drove and I coded. Collaborated on RTK query

## Thur, Aug 24, 2023

Today I continued work on the Navigation Bar, worked with the team in a collaborative manner to address main page redux core functionality to main page.

## Fri, Aug 25, 2023

## Mon, Aug 28, 2023

Today I worked on the Project Detail Page and Project Detail components. Added a CSS functionality for front end beautification.

## Tue, Aug 29, 2023

Today I worked on the Task Detail View functionality component. Adding that component into the Project Detail view.

## Wed, Aug 30, 2023

Today I worked on Login function and added to Main. Created a Login Form and Login Component. I also worked on logout functionality and component.

## Thur, Aug 31, 2023

- Today I worked on "Create Task" component and added the component to the "Main" page.

## Tue, Sept 5, 2023

- We worked on Delete task function, added delete Project function to the "Main" page.

## Wed, Sept 6, 2023

Today, I worked on the following:

Created a ProjectsPage so users are able to see all of their projects in one locations.Prior to this feature, there was no place for a user to see more then 4 projects cards. Now a user has a project management page which will allow the user to create a new project, and navigate to the project detail page for that project to view tasks and modify. We used the "projectCard" component to make this page work.

We also deleted unnecessary pages such as "Tasks" and "Profile".

## Thur, Sept 7, 2023

Today, I worked on the following:

Unit test for get task.
Several front-end navigation and styling updates which include the following. Creation of new project <button> in Project Page, Project Page, Landing Page, Sign up Page, and Login page styling to conform to remaining of applications. Also included several navigation links through-out these pages for UX improvement.

## Fri, Sept 8, 2023

Today, I worked on the following:
Worked on numerous front end UX customizations to improve on user experience. Loading of the task details once a task is created
Task Detail card, <description> field, the text is not wrapping. <Project> section of the page has not been refactored to look in line with the rest of the page. Worked on the ReadMe.md document and updated **_Get a User's Specific Project_**, **_Post a User's Project_**

## Mon, Sept 11, 2023

Today, I worked on the following:
Several improvements to the project detail page, including exporting the edit project component into its own card on the page, fixed edit project component which was causing an error.
