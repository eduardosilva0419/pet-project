# Paul's Journal

## Monday 20230814

Got repo. Installed redux-toolkit and tailwind. git practice. Started auth.

## Tuesday 20230815

completed auth.

## Wednesday 20230816

made migrations for projects and tasks. started projects api.

## Thursday 20230817

did task models and create method.

## Friday 20230818

did the api/projects/{project_id}/tasks endpoint

## Monday 20230821

started working on the navigation bar

## Tuesday 20230822

finished the navbar layout so we can update when it needs updating. Got the redux store and rtk query set up.

## Wednesday 20230823

a new user can now create an account and the get token end point is working. navbar is done. implemented react-router-dom.

## Thursday 20230824

worked on various things

## Friday 20230825

worked on various bugs

## Monday 20230828

started working on the weekview

## Tuesday 20230829

dealt with a bunch of date and datetime stuff today. not fun. but learned a lot.

## Wednesday 20230830

finished working on the weekview

## Thursday 20230831

made various minor bug fixes

## Tuesday 20230905

made the edit task component

## Wednesday 20230906

completed my unit test. fixed rtk query issues. fixed create task bug

## Thursday 20230907

took a more mentor type role today so others could catch up on commits helping with issues that came up

## Friday 20230908

finished wrapping up some slight css issues

## Monday 20230911

wrapped up and submitted everything
