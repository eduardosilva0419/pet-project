## August 14, 2023

Today, I worked on:

* Setting up Tailwind and Authentication

Paul, Joe, Eduardo, and I started writing some Python to set up the project. We worked on Docker-Compose.yaml. We also set up the Tailwind part of the project for better aesthetics. We also added directories and files to our project structure to be able to use PostgreSQL.

## August 15, 2023

Authentication.

## August 16th, 2023

Today I worked with my group to make migrations on our api. We started with the models and this will define the basis of our project.

## August 17th, 2023

Kept working on models and made migrations

## August 18th, 2023

We worked on endpoints and started th profile page backend

## August 21st, 2023

Pair programmed with Joseph. We worked on the front end for the landing page.

## August 22nd, 2023

Pair programmed with Joseph. Worked with Joseph on landing page.

## August 23rd, 2023

Pair programmed with Joseph. Worked on sign up sheet.

## August 24th, 2023
Pair programmed with Joseph. Worked on
Sign in

## August 25th, 2023
Worked with Joseph on Sign in sheet.

## August 28th, 2023
Pair programmed with Eduardo. Project detail page and project detail components. CSS features were added.

## August 29th, 2023
Pair programmed with Eduardo. Task detail page.

## August 30th, 2023
Pair programmed with Eduardo. Login.

## August 31st, 2023
Worked with Eduardo on Create task.

## September 5th, 2023
Delete task and delete project

## September 6th, 2023
Added projectspage and deleted profile

## September 7th, 2023

Worked on unit test

## September 8th, 2023
Worked on CSS for landing page, App.js added a background and removed white background from forms.

## September 11th, 2023
Merged CSS work and completed project
