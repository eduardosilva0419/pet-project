# To run:

Clone to your machine

update the example.env and rename to .env

docker volume create pet-project-data

docker compose build

docker compose up

run migrations in fastapi container terminal

python -m migrations up

# Pet Project

## About Pet Project

Pet Project is a project management app for people.

## Wire-frame Diagrams

Figma diagram can be found here: https://www.figma.com/file/Hs78UwXA6HE8yGyZH0uJHT/Pet-Project?type=design&node-id=0-1&mode=design&t=HxG03LBTBHB6DBKV-0

## API Documentation

### Projects

**_Get all of a Users Projects_**

> /api/projects/mine

**GET method** - get all of users projects

The get request to this endpoint retrieves all projects for the authenticated user. User must be logged in. A successful response includes an object with a key of "projects" and a value that contains a list of project objects.

Example Response:

```
{
  "projects": [
    {
      "title": "House Chores",
      "description": "House Chores",
      "due_date": null,
      "id": 1,
      "created": "2023-09-06"
    },
    {
      "title": "API Docs",
      "description": "Learn to write API documentation",
      "due_date": null,
      "id": 2,
      "created": "2023-09-07"
    }
  ]
}
```

**_Post a User's Project_**

> /api/projects/mine

**POST method** - create a new project for user

Upon successfully creating a new project, the server responds with an object representing the newly created project, including the provided data and additional information:

Example payload:

```
{
  "title": "string",
  "description": "string",
  "due_date": "2023-09-07"
}
```

Example Successful Response:

```
{
  "title": "string",
  "description": "string",
  "due_date": "2023-09-07",
  "id": 0,
  "created": "2023-09-07"
}
```

**_Get a User's Specific Project_**

> /api/projects/{project_id}

**GET project method** - get specific project by id

The GET request to this endpoint retrieves a specific project based on its unique identifier (project_id). To access project details, the user must provide the following data in the request payload:

Example payload:

```
{
  "project_id": [
    {
      "id": 13,
    },
  ]
}
```

Upon a successful request, the server responds with an object representing the specified project, including its details:

Example Successful Response:

```
{
  "title": "Guitar",
  "description": "Learn to read sheet music
  "due_date": "2023-09-07",
  "id": 13,
  "created": "2023-09-07"
}
```

**_PUT a User's Specific Project_**

> /api/projects/{project_id}

**PUT project method** - Update a specific project by id

The PUT method allows you to modify specific details of a project by specifying its ID and providing the new values for the title, description, and due date.

Example payload:

```
{
  "project_id": [
  {
  "title": "string",
  "description": "string",
  "due_date": "2023-09-11"
  }
  ]
}
```
Upon a successful update, this response format provides comprehensive information about the updated project, including its title, description, due date, unique ID, and the timestamp of the update. It serves as confirmation that the project has been successfully modified according to the provided input.

Example Successful Response:

```
{
  "title": "string",
  "description": "string",
  "due_date": "2023-09-11",
  "id": 0,
  "created": "2023-09-11"
}
```

**_DELETE a User's Specific Project_**

> /api/projects/{project_id}

**DELETE project method** - Delete a specific project by id

The DELETE method uses a specific project ID in order to delete that project.

Example payload:

```
{
  "project_id": 13
}
```
Upon successful deletion, the project will no longer be viewable and therefore the frontend will not have that same project ID to display for the user, in turn removing the project from the list of projects.

Example Successful Response:

```
{
  "message": "Project not found"
}
```

**_Post a User's Task_**

> /api/tasks

**POST method** - create a new task for user

After successfully adding a new task, the server returns an object that represents the newly-created task, complete with the data you supplied and other additional details.

Example Value:

```
{
  "title": "string",
  "description": "string",
  "due_date": "2023-09-11T22:18:09.766Z",
  "is_completed": true,
  "project_id": 0
}
```

Response Body:
```
{
  "title": "string",
  "description": "string",
  "due_date": "2023-09-11T22:20:41.925000+00:00",
  "is_completed": true,
  "project_id": 0,
  "id": 8,
  "created": "2023-09-11"
}
```

**_Get all User's Tasks_**

> /api/tasks/mine

**GET method** - get all of users tasks

When an authenticated user sends a GET request to this endpoint, it fetches all the user's tasks. The user must be logged in to do so. A successful reply from the server will include an object that has a "tasks" key, whose value is a list of task objects.

Example value:
```
{
  "tasks": [
    {
      "title": "Strum Learning",
      "description": "Read Book",
      "due_date": "2023-08-18T17:07:31.959000",
      "is_completed": true,
      "project_id": 1,
      "id": 2,
      "created": "2023-08-18"
    },
    {
      "title": "Karaoke",
      "description": "w",
      "due_date": "2023-08-31T02:18:00",
      "is_completed": false,
      "project_id": 1,
      "id": 5,
      "created": "2023-08-31"
    },
  ]
}
```

**_Get a User's Task_**

> /api/tasks/{task_id}

**GET method** - get specific project by id

A GET request to this endpoint fetches details for a particular task, identified by its unique 'task_id'. To obtain the information about the task, the user must include specific data in the request's value.

Example value:
```
{
  "task_id": [
    {
      "id": 1,
    },
  ]
}
```

Example Response:
```
{
  "title": "Strum Learning",
  "description": "Read Book",
  "due_date": "2023-09-07T04:12:00",
  "is_completed": true,
  "project_id": 1,
  "id": 1,
  "created": "2023-08-18"
}
```

**_Update a User's Task_**

> /api/tasks/{task_id}

**PUT task method** - Update a specific task by id

Using the PUT method, you can update specific attributes of a task, such as its title, description, due date, completion status, and project id, by identifying the task with its unique ID and supplying the new values.

Example Value:
```
{
  "title": "string",
  "description": "string",
  "due_date": "2023-09-11T22:34:59.987Z",
  "is_completed": true,
  "project_id": 0
}
```

Example Response:
```
{
  "title": "string",
  "description": "string",
  "due_date": "2023-09-11T22:36:09.516Z",
  "is_completed": true,
  "project_id": 0,
  "id": 0,
  "created": "2023-09-11"
}
```

**_DELETE a User's Specific Task_**

> /api/tasks/{task_id}

**DELETE project method** - Delete a specific task by id

The DELETE method uses a specific task ID to delete a task.

Example Value:
```
{
  "task_id": [
    {
      "id": 8,
    },
  ]
}
```

Example Response:
```
{
  "message": "Task deleted"
}
```

**_GET a User's Task for Project_**

> /api/projects/{project_id}/tasks

**GET My Tasks For Project** - Get all tasks for a project

the GET method uses a project ID to fetch all tasks assigned to the project.

Example Value:
```
{
  "project_id": [
    {
      "id": 1,
    },
  ]
}
```

Example Response:
```
{
  "title": "Guitar",
  "description": "string",
  "due_date": null,
  "id": 1,
  "created": "2023-08-17",
  "tasks": [
    {
      "title": "Strum Learning",
      "description": "Read Book",
      "due_date": "2023-08-18T17:07:31.959000",
      "is_completed": true,
      "project_id": 1,
      "id": 3,
      "created": "2023-08-18"
    },
  ]
}
```








### Accounts


**POST method** - create a new Account

After successfully adding a new account, the server will log the user in under those credintials associating an access token with their account.

Example Value:

```
{
  "username": "string",
  "password": "string"
}
```

Response Body:
```
{
  "access_token": "string",
  "token_type": "Bearer",
  "account": {
    "id": "string",
    "username": "string"
  }
}
```



**GET method** - get current Token

The get method returns any tokens associated with the user currently.

Response Body:
```
{
  "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxMDFjOGQ2OS1lYjM4LTRiOWQtYmM3Ny0yNmI1OTA5MzFiNmIiLCJleHAiOjE2OTQ0NzU5MjcsInN1YiI6ImJvYiIsImFjY291bnQiOnsiaWQiOiIyIiwidXNlcm5hbWUiOiJib2IifX0.wR6uK9v6ieCfjXoUQISuckNifJkzrI-vRtiNonBG5PE",
  "token_type": "Bearer",
  "account": {
    "id": "2",
    "username": "bob"
  }
}
```

**POST method** - Login

The Post method logs in any user allowing them an access token, only if their account is already registed.

Example Value:
```
{
  "username": "string",
  "password": "string"
}
```

Response Body:
```
{
  "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI1YTUzZWU3OC03MWMyLTRkMjUtYTBmMC1kMGQxNjFkNDg3MzgiLCJleHAiOjE2OTQ0NzgyMTYsInN1YiI6ImpvIiwiYWNjb3VudCI6eyJpZCI6IjEiLCJ1c2VybmFtZSI6ImpvIn19.UxndioukzcQlIApZT7waDM8WOg2l4e8PstU6A_5xI20",
  "token_type": "Bearer"
}
```

**DELETE method** - Logout

The Delete method logs the current user out, abandoning their token.

Response Body:
```
true
```
### Team

- Julio Gomez
- Paul Johns @johnnycodes
- Joseph Marcello @likidswrld
- Eduardo Silva @eduardosilva0419
